# Spike Report

## Graphic Programming - Indoor Lighting

### Introduction

This spike aims at building lighting for an indoor scene, like a house or an apartment and having some resemblance to realistic lighting.

### Goals

1. The Spike Report should answer each of the Gap questions
1. A small indoor scene, with light coming in from the windows, and a three-point lighting setup for a high-poly �feature� model placed upon a table in the middle of the room (get one online).
    1. Start using the Lighting Quick Start Guide
    1. Have most objects Static
    1. Have the feature object Movable (so the player can knock it around by walking into it).
    1. Use at least one Point Light and one Spot Light
    1. Have at least one smooth and/or metallic object, and use a Reflection Capture (box or sphere) to aid in reflection.
    1. Use a Light Profile on as many lights as plausible.
    1. For the windows, create two copies of the level.
        1. One will use light-cards
        1. One will use Lightmass portals
        1. Write a few sentences about how to use each of them, and what the differences are.

### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Bradley
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* [Setting up the scene](https://docs.unrealengine.com/latest/INT/Engine/Rendering/LightingAndShadows/QuickStart/1/index.html)
* [high-poly �feature� model](https://www.cgtrader.com/items/773837/download-page)
* [3 point lighting](http://www.kembadesign.com/tutorials/2016/6/10/how-to-set-up-three-point-lighting-in-unreal-engine-412)
* [Creating Light Cards](https://www.unrealengine.com/en-US/blog/simulating-area-lights-in-ue4)
* [Light profiles](http://www.lithonia.com/photometrics.aspx)

### Tasks Undertaken

1. Complete the 'setting the scene' tutorial to create the level for the spike.
    1. Once complete make the house bigger and add more windows.

1. Get a high poly model (can use the one in the resources section) and put it on display.
    1. Set up 3 point lighting system on the model.
    1. It needs to have a key light that is placed on the front left side of the model.
    1. A fill light that sits on the far front right side of the model.
    1. A backlight that sits diagnal with the key light.

1. Place multiple point lights/spot lights and place different light profiles on them to see the different effects.
    1. Can get more profiles from the link provided in the resources

1. Create a metallic material or use a material from the starter content place on a 400x 400 wall, skrink it and add a reflective sphere in front of it to make it reflect the room.

1. Create two levels that will have a lightmass portal and another one that will have the lightcards
    1. Compare between the two and how theu effect the scene.

### What we found out

1. The difference between the light mass portals and the light cards is that the light mass portal helps the light filter in through windows, doors, any type of light opening, whereas, the light cards create a 'false' light to help project light into those openings. It would be an assumation that the light portals and lightcards would be used together to create the realistic lighting.

1. A 3 point lighting system is a lighting method and 'its purpose is to properly illuminate a subject in an effective way that is not only pleasing to the eyes but also relatively simple in its approach' - [Pluralsight](https://www.pluralsight.com/blog/film-games/understanding-three-point-lighting). When using the 3 point lighting method in a game engine, it would be most commonly used in cinemtatics or cut scenes to highlight the characters and have some sort of natural lighting the world.

### Open Issues/Risks

1. Was unable to complete the moveable object for the player to knock it around. Would advise to move to another spike to be completed.

1. Creating a 3 point light system in Unreal is difficult as lights over lap each other and cancel the light effects of one of the lights.

### Recommendations

1. In building 3 point light, its best to have someone who is skilled with it, to set it up correctly.